<?php
global $wpdb;
$table_name = $wpdb->prefix.'file_manager';
$file_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name",""));

$table_name = $wpdb->prefix.'categories';
$manufacturer_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Manufacturer'",""));

$product_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Product'",""));

$series_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Series'",""));

$model_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Model'",""));

$type_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Type'",""));


?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<div class="container">
	<h2>Manage Files</h2>
	<div class="alert alert-success succ" style="display: none;"></div>
	<div class="alert alert-danger err" style="display: none;"></div>
	<a class="btn btn-success pull-right" onclick="showModal()">Add File</a>
	<table class="table table-reponsive">
		<thead>
			<th>Id</th>
			<th>Manufacturer</th>
			<th>Product</th>
			<th>Series</th>
			<th>Model</th>
			<th>Type</th>
			<th>Title</th>
			<th>Extension</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php foreach ($file_data as $key => $value) {
			    $manufacturer_cat = "'".$value->manufacturer_cat."'";    
				$product_cat = "'".$value->product_cat."'";
				$series_cat = "'".$value->series_cat."'";
				$model_cat = "'".$value->model_cat."'";
				$type_cat = "'".$value->type_cat."'";
				$file_ext = "'".$value->file_ext."'";
				$file_title = "'".$value->file_title."'";
				$file_link = "'".$value->file_link."'";

				echo '<tr>';
				echo '<td>'.$value->file_id.'</td>';
				echo '<td>'.$value->manufacturer_cat.'</td>';
				echo '<td>'.$value->product_cat.'</td>';
				echo '<td>'.$value->series_cat.'</td>';
				echo '<td>'.$value->model_cat.'</td>';
				echo '<td>'.$value->type_cat.'</td>';
				echo '<td><a href="'.$value->file_link.'">'.$value->file_title.'</a></td>';
				echo '<td>'.$value->file_ext.'</td>';
				echo '<td><a class="btn btn-sm btn-info" onclick="editFile('.$value->file_id.','.$manufacturer_cat.','.$product_cat.','.$series_cat.','.$model_cat.','.$type_cat.','.$file_ext.','.$file_title.', '.$file_link.')">Edit</a>&nbsp;&nbsp;<a class="btn btn-sm btn-danger" onclick="deleteFile('.$value->file_id.')">Delete</a></td>';
			} ?>
			<tr></tr>
		</tbody>
	</table>
	

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Category</h4>
      </div>
      <div class="modal-body">
      	<div></div>
      	<input type="hidden" name="file_id" id="file_id" value="0">
        <label>Select Manufacturer: </label>
        <select name="manufacturer" class="form-control" id="manufacturer_cat">
        	<?php foreach ($manufacturer_data as $key => $value) {
        		echo '<option value="'.$value->sub_cat.'">'.$value->sub_cat.'</option>';
        	} ?>
        </select>
        <label>Select Product: </label>
        <select name="product" class="form-control" id="product_cat">
        	<?php foreach ($product_data as $key => $value) {
        		echo '<option value="'.$value->sub_cat.'">'.$value->sub_cat.'</option>';
        	} ?>
        </select>
        <label>Select Series: </label>
        <select name="series" class="form-control" id="series_cat">
        	<?php foreach ($series_data as $key => $value) {
        		echo '<option value="'.$value->sub_cat.'">'.$value->sub_cat.'</option>';
        	} ?>
        </select>
        <label>Select Model: </label>
        <select name="model" class="form-control" id="model_cat">
        	<?php foreach ($model_data as $key => $value) {
        		echo '<option value="'.$value->sub_cat.'">'.$value->sub_cat.'</option>';
        	} ?>
        </select>
        <label>Select Type: </label>
        <select name="type" class="form-control" id="type_cat">
        	<?php foreach ($type_data as $key => $value) {
        		echo '<option value="'.$value->sub_cat.'">'.$value->sub_cat.'</option>';
        	} ?>
        </select>
        <br>
        <label>File Title</label>
        <input type="text" name="file_title" id="file_title" class="form-control" placeholder="File title">
        <br>
        <label>File Link</label>
        <input type="text" name="file_link" id="file_link" class="form-control" placeholder="file link">
        <br>
        <label>File Extension</label>
        <select name="file_ext" id="file_ext" class="form-control">
            <option value="docx">Docx</option>
            <option value="pdf">PDF</option>
            <option value="jpg">JPG</option>
            <option value="png">PNG</option>
            <option value="tiff">Tiff</option>
            <option value="dwg">DWG</option>
            <option value="xlsx">XLSX</option>
            <option value="cdr">CDR</option>
            <option value="rfa">RFA</option>
            <option value="ai">Ai</option>
            <option value="eps">EPS</option>
            <option value="zip">Zip</option>
        </select>
        
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" onclick="saveFile()">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<script>
	function showModal(){
		jQuery("#cat_id").val(0);
		jQuery("#main_cat").val("Manufacturer");
		jQuery("#sub_cat").val("");
		jQuery("#myModal").modal("show");
	}
	

	function saveFile(){
		var file_title = jQuery("#file_title").val();
		var file_link = jQuery("#file_link").val();
		var file_ext = jQuery("#file_ext").val();
		if(file_title == "" || file_ext == "" || file_link == ""){
			alert("Please enter all fields");
			return false;
		}
		var file_id = jQuery("#file_id").val();
		var manufacturer_cat = jQuery("#manufacturer_cat").val();
		var product_cat = jQuery("#product_cat").val();
		var series_cat = jQuery("#series_cat").val();
		var model_cat = jQuery("#model_cat").val();
		var type_cat = jQuery("#type_cat").val();

		jQuery.ajax({
			type: 'POST',   
			url: ajaxurl, 
			data: {"action": "wpcraft_save_file", "file_id": file_id, "file_title": file_title, "file_link": file_link, "file_ext": file_ext, "manufacturer_cat": manufacturer_cat, "product_cat": product_cat, "series_cat": series_cat, "model_cat": model_cat, "type_cat": type_cat},
			success: function(data) {
				jQuery('.ajax-loader').hide();
				if (data == 'done') {
					jQuery("#myModal").modal("hide");
					jQuery(".succ").text('Successfully Added');
					jQuery(".succ").show();
					
					setTimeout(function(){
						location.reload();
					}, 2000);
				}else if (data == "found") {
					alert('File Already Exists');
				}else{
					alert("Error Occured");
				}
			}
		});
	}

	function editFile(id, manufacturer_cat, product_cat, series_cat, model_cat, type_cat, file_ext, file_title, file_link){
		jQuery("#file_id").val(id);
		jQuery("#manufacturer_cat").val(manufacturer_cat);
		jQuery("#product_cat").val(product_cat);
		jQuery("#series_cat").val(series_cat);
		jQuery("#model_cat").val(model_cat);
		jQuery("#type_cat").val(type_cat);
		jQuery("#file_link").val(file_link);
		jQuery("#file_title").val(file_title);
		jQuery("#file_ext").val(file_ext);
		jQuery('#myModal').modal('show'); 
	}

	function deleteFile(id){
		if(confirm("Are you sure?")){
			jQuery.ajax({
				type: 'POST',   
				url: ajaxurl, 
				data: {"action":"wpcraft_delete_file","id":id},
				success: function(data) {
					jQuery('.ajax-loader').hide();
					if (data == "done") {
						jQuery("#" + id).remove();
						setTimeout(function(){
							jQuery(".succ").text('Successfully Deleted');
							jQuery(".succ").show();
						},2500);

					}else{
						jQuery(".err").text('Error Occured');
						jQuery(".err").show();
					}
				}
			});
		}
	}
</script>
	
</div>
