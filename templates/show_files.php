<style type="text/css">
    .grid-container{
        padding-right: 1.875rem;
        padding-left: 1.875rem;
        max-width: 87.5rem;
        margin-left: auto;
        margin-right: auto;
    }
    section {
        position: relative;
        z-index: 1;
    }
    .filter .grid-x {
        position: relative;
        overflow: hidden;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        /*justify-content: space-between;*/
    }
    .grid-margin-x {
        margin-left: -1.875rem;
        margin-right: -1.875rem;
    }
    .grid-x {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
    }
    section.filter .grid-x .cell.medium-3 {
        margin-top: 0 !important;
    }
    .filter .cell {
        position: relative;
    }
    .grid-margin-x > .cell {
        margin-left: 0.875rem;
        margin-right: 0.875rem;
    }
    section h4.header {
        font-size: 16px;
        padding: 10px 20px;
        background-color: #404041;
        margin-bottom: 0;
        color: #fefefe;
        font-weight: 600;
    }
    div.product-select {
        padding: 0;
        min-height: 250px;
        background-color: #f2f2f2;
        border-radius: 0;
        border: 1px solid #cacaca;
        font-family: "Raleway", "Helvetica Neue", "Helvetica", "Arial", "Sans-serif" !important;
        height: 250px;
        overflow-y: scroll;
    }
    .medium-3{
        width: 17%;
    }
    
    @media screen and (max-width: 800px) {
        .medium-3{
            width: 42%;
        }   
    }
    
    @media screen and (max-width: 480px) {
        .medium-3{
            width: 92%;
        }   
    }

    .table.list tr:nth-child(odd){
        background-color: #eef4f9;
    }
    .table.list tr:nth-child(even){
        background-color: #f1f1f1;
    }
    .table.list tbody{
        border: 1px solid #f1f1f1;
        border-top: 2px solid #d4dce3;
    }
    .table.list td {
        border: none;
        padding: 10px;
        text-align: left;
    }
    .fman-selected{
        background-color: #404041c2;
        color: #f2f2f2; 
    }
    .product-select li{
        font-size: 16px;
        line-height: 1.4;
        padding: 10px 20px;
        margin: 0;
        cursor: pointer !important;
        list-style: none;
    }
    ul.no-bullet{
        margin: 0;
        padding: 0 !important;
    }
    .table.list.brochures table{
        width: 100%;
        font-family: "Raleway", "Helvetica Neue", "Helvetica", "Arial", "Sans-serif" !important;
    }
    .background-loader {
        background-color: hsl(0, 15%, 90%);
        display: block;
        height: 100%;
        left: 0;
        opacity: 0.3;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 10000;
    }
    .image-laoding {
        border-radius: 3px;
        height: auto;
        left: 50%;
        opacity: 0.99;
        padding: 3px;
        position: fixed;
        top: 40%;
        width: auto;
        z-index: 1;
    }
    section.results {
        padding-top: 25px;
    }
</style>

<?php 

global $wpdb;
$table_name = $wpdb->prefix.'categories';
$manufacturer_data = $wpdb->get_results($wpdb->prepare("SELECT sub_cat FROM $table_name WHERE main_cat = 'Manufacturer' ORDER BY sort_order ASC",""));


// $table_name = $wpdb->prefix.'file_manager';
// $_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name",""));

?>

<section class="filter grid-container">
    <div class="grid-x grid-margin-x">
        <div class="cell medium-3">
            <div class="loader ready"></div>
            <h4 class="header">Manufacturer</h4>
            <div class="form-control product-select" name="lines">
                <ul class="no-bullet">
                    <?php
                    $prod = 'manufacturer';
                    $prod = "'".$prod."'";
                    foreach ($manufacturer_data as $key => $value) {
                        $cat = "'".$value->sub_cat."'";
                        echo '<li class="lines-select fman-list-manufacturer" onclick="showNextCategories(this, '.$prod.','.$cat.', 0, 0, 0, 0)">'.$value->sub_cat.'</li>';
                    } ?>
                </ul>
            </div>
        </div>
        <div class="cell medium-3">
            <div class="loader ready"></div>
            <h4 class="header">Product</h4>
            <div class="form-control product-select" name="lines">
                <ul class="no-bullet" id="product">
                    
                </ul>
            </div>
        </div>
        <div class="cell medium-3">
            <h4 class="header">Series</h4>
            <div class="form-control product-select" name="series">
                <ul class="no-bullet" id="series">
                    
                </ul>
            </div>
        </div>
        <div class="cell medium-3">
            <h4 class="header">Model</h4>
            <div class="form-control product-select" name="models">
                <ul class="no-bullet" id="models">
                    
                </ul>
            </div>
        </div>
        <div class="cell medium-3">
            <h4 class="header">Type</h4>
            <div class="form-control product-select" name="types">
                <ul class="no-bullet" id="type">
                    
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="results grid-container">
    <div class="grid-y">
        <div class="cell">
            <div class="text">
                <h3>Your Downloadable File(s)</h3>
            </div>
        </div>
        <div class="cell">
            <div class="table list brochures">
                <table>
                    <thead></thead>
                    <tbody id="document-list">
                    </tbody>
                    <tfoot></tfoot>
                </table>
            </div>
        </div>
    </div>
</section>

<div class="ajax-loader" style="display:none;">
    <div class="image-laoding">
        <img src="<?php echo FMAN_URL ?>images/loader.gif" title="processing..." alt="loading..."/>
    </div>
    <div class="background-loader"></div>
</div>

<script type="text/javascript">
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
 
    function showNextCategories(el, type, manufacturer_data, product_data, series_data, models_data, type_data){
        jQuery('.ajax-loader').show();
        jQuery("#document-list").html("");
        if (type == "manufacturer") {
            jQuery(".fman-list-manufacturer").removeClass("fman-selected");
            jQuery("#product").html("");
            jQuery("#models").html("");
            jQuery("#type").html("");            
            jQuery("#series").html("");  
        } else if (type == "product") {
            jQuery(".fman-list-product").removeClass("fman-selected");
            jQuery("#models").html("");
            jQuery("#type").html("");            
            jQuery("#series").html("");            
        } else if (type == "series") {
            jQuery(".fman-list-series").removeClass("fman-selected");
            jQuery("#models").html("");
            jQuery("#type").html("");
        }else if (type == "model") {
            jQuery(".fman-list-model").removeClass("fman-selected");
            jQuery("#type").html("");
        }else {
            jQuery(".fman-list-type").removeClass("fman-selected");
        }
        jQuery(el).addClass("fman-selected");
        jQuery.ajax({
            type: 'POST',   
            url: ajaxurl, 
            data: {"action":"wpcraft_get_sub_categories","type":type, "manufacturer_data": manufacturer_data, "product_data":product_data, "series_data":series_data, "models_data":models_data,"type_data":type_data},
            success: function(data) {
                if (type == "manufacturer") {
                    jQuery("#product").html(data);  
                } else if (type == "product") {
                    jQuery("#series").html(data);
                } else if (type == "series") {
                    jQuery("#models").html(data);
                }else if (type == "model") {
                    jQuery("#type").html(data);
                }else{
                    jQuery("#document-list").html(data);
                }
                jQuery('.ajax-loader').hide();
            }
        });
    }
</script>