<?php
global $wpdb;
$table_name = $wpdb->prefix.'categories';
$manufacturer_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Manufacturer' ORDER BY sort_order ASC",""));

$product_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Product' ORDER BY sort_order ASC",""));

$series_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Series' ORDER BY sort_order ASC",""));

$model_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Model' ORDER BY sort_order ASC",""));

$type_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE main_cat = 'Type' ORDER BY sort_order ASC",""));

?>

<style type="text/css">
	/* Style the tab */
    .tabupper {
      overflow: hidden;
      border: 1px solid #ccc;
      background-color: #f1f1f1;
    }
    
    /* Style the buttons inside the tab */
    .tabupper button {
      background-color: inherit;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      transition: 0.3s;
      font-size: 17px;
    }
    
    /* Change background color of buttons on hover */
    .tabupper button:hover {
      background-color: #ddd;
    }
    
    /* Create an active/current tablink class */
    .tabupper button.activeques {
      background-color: #ccc;
    }
    
    /* Style the tab content */
    .tabcontentques {
      display: none;
      padding: 6px 12px;
      border: 1px solid #ccc;
      border-top: none;
    }
    .panel-heading{
    	background: #f1f1f1;
    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="container">
	<h2>Manage Categories</h2>
	<div class="alert alert-success succ" style="display: none;"></div>
	<div class="alert alert-danger err" style="display: none;"></div>
	<div class="tabupper">
	    <button class="tablinks activeques" onclick="openCity(event, 'Manufacturer')">Manufacturer</button>
		<button class="tablinks" onclick="openCity(event, 'Product')">Product</button>
		<button class="tablinks" onclick="openCity(event, 'Series')">Series</button>
		<button class="tablinks" onclick="openCity(event, 'Model')">Model</button>
		<button class="tablinks" onclick="openCity(event, 'Type')">Type</button>
		<button class="tablinks pull-right" onclick="showModal()">Add Category</button>
	</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Category</h4>
      </div>
      <div class="modal-body">
      	<div></div>
      	<input type="hidden" name="cat_id" id="cat_id" value="0">
        <label>Select Category: </label>
        <select name="main_cat" class="form-control" id="main_cat">
            <option value="Manufacturer">Manufacturer</option>
        	<option value="Product">Product</option>
        	<option value="Series">Series</option>
        	<option value="Model">Model</option>
        	<option value="Type">Type</option>
        </select>
        <br>
        <label>Category Title</label>
        <input type="text" name="sub_cat" id="sub_cat" class="form-control" placeholder="Category title">
        <br>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" onclick="saveCategory()">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="Manufacturer" class="tabcontentques" style="display: block;">
	<table class="table table-responsive">
		<thead>
			<th>Id</th>
			<th>Caegory</th>
			<th>Action</th>
		</thead>
		<tbody class="sortable">
			<?php foreach ($manufacturer_data as $key => $value) {
				echo '<tr id="'.$value->category_id.'">';
				$main_cat = "'".$value->main_cat."'";
				$sub_cat = "'".$value->sub_cat."'";
				echo '<td>'.$value->category_id.'</td>';
				echo '<td>'.$value->sub_cat.'</td>';
				echo '<td><a class="btn btn-sm btn-info" onclick="editCategory('.$value->category_id.', '.$main_cat.', '.$sub_cat.')">Edit</a>&nbsp;&nbsp;<a class="btn btn-sm btn-danger" onclick="deleteCategory('.$value->category_id.')">Delete</a></td>';
				echo '</tr>';
			} ?>
		</tbody>
	</table>
</div>

<div id="Product" class="tabcontentques" style="display: none;">
	<table class="table table-responsive">
		<thead>
			<th>Id</th>
			<th>Caegory</th>
			<th>Action</th>
		</thead>
		<tbody class="sortable">
			<?php foreach ($product_data as $key => $value) {
				echo '<tr id="'.$value->category_id.'">';
				$main_cat = "'".$value->main_cat."'";
				$sub_cat = "'".$value->sub_cat."'";
				echo '<td>'.$value->category_id.'</td>';
				echo '<td>'.$value->sub_cat.'</td>';
				echo '<td><a class="btn btn-sm btn-info" onclick="editCategory('.$value->category_id.', '.$main_cat.', '.$sub_cat.')">Edit</a>&nbsp;&nbsp;<a class="btn btn-sm btn-danger" onclick="deleteCategory('.$value->category_id.')">Delete</a></td>';
				echo '</tr>';
			} ?>
		</tbody>
	</table>
</div>

<div id="Series" class="tabcontentques">
  <table class="table table-responsive">
		<thead>
			<th>Id</th>
			<th>Caegory</th>
			<th>Action</th>
		</thead>
		<tbody class="sortable">
			<?php foreach ($series_data as $key => $value) {
				echo '<tr id="'.$value->category_id.'">';
				$main_cat = "'".$value->main_cat."'";
				$sub_cat = "'".$value->sub_cat."'";
				echo '<td>'.$value->category_id.'</td>';
				echo '<td>'.$value->sub_cat.'</td>';
				echo '<td><a class="btn btn-sm btn-info" onclick="editCategory('.$value->category_id.', '.$main_cat.', '.$sub_cat.')">Edit</a>&nbsp;&nbsp;<a class="btn btn-sm btn-danger" onclick="deleteCategory('.$value->category_id.')">Delete</a></td>';
				echo '</tr>';
			} ?>
		</tbody>
	</table>
</div>

<div id="Model" class="tabcontentques">
	<table class="table table-responsive">
		<thead>
			<th>Id</th>
			<th>Caegory</th>
			<th>Action</th>
		</thead>
		<tbody class="sortable">
			<?php foreach ($model_data as $key => $value) {
				echo '<tr id="'.$value->category_id.'">';
				$main_cat = "'".$value->main_cat."'";
				$sub_cat = "'".$value->sub_cat."'";
				echo '<td>'.$value->category_id.'</td>';
				echo '<td>'.$value->sub_cat.'</td>';
				echo '<td><a class="btn btn-sm btn-info" onclick="editCategory('.$value->category_id.', '.$main_cat.', '.$sub_cat.')">Edit</a>&nbsp;&nbsp;<a class="btn btn-sm btn-danger" onclick="deleteCategory('.$value->category_id.')">Delete</a></td>';
				echo '</tr>';
			} ?>
		</tbody>
	</table>
</div>

<div id="Type" class="tabcontentques">
	<table class="table table-responsive">
		<thead>
			<th>Id</th>
			<th>Caegory</th>
			<th>Action</th>
		</thead>
		<tbody class="sortable">
			<?php foreach ($type_data as $key => $value) {
				echo '<tr id="'.$value->category_id.'">';
				$main_cat = "'".$value->main_cat."'";
				$sub_cat = "'".$value->sub_cat."'";
				echo '<td>'.$value->category_id.'</td>';
				echo '<td>'.$value->sub_cat.'</td>';
				echo '<td><a class="btn btn-sm btn-info" onclick="editCategory('.$value->category_id.', '.$main_cat.', '.$sub_cat.')">Edit</a>&nbsp;&nbsp;<a class="btn btn-sm btn-danger" onclick="deleteCategory('.$value->category_id.')">Delete</a></td>';
				echo '</tr>';
			} ?>
		</tbody>
	</table>
</div>

<script>


    jQuery( function() {
    	jQuery( ".sortable" ).sortable({
    		start: function(e, ui) {
    	    }, 
    		update: function( event, ui ) {
    			var order = jQuery(this).sortable('toArray');
    			jQuery.ajax({
    				type: 'POST', 
    				url: ajaxurl,
    				data: {"action": "wpcraft_update_categories_sortorder","fieldids":order}, 
    				success: function(data){ 
    				}
    			});                                                            
    		}
    	});
    	jQuery( ".sortable" ).disableSelection();
    });


	function showModal(){
		jQuery("#cat_id").val(0);
		jQuery("#main_cat").val("Manufacturer");
		jQuery("#sub_cat").val("");
		jQuery("#myModal").modal("show");
	}
	function openCity(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontentques");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" activeques", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " activeques";
	}

	function saveCategory(){
		var sub_cat = jQuery("#sub_cat").val();
		if(sub_cat == ""){
			alert("Please enter category title");
			return false;
		}
		var main_cat = jQuery("#main_cat").val();
		var cat_id = jQuery("#cat_id").val();

		jQuery.ajax({
			type: 'POST',   
			url: ajaxurl, 
			data: {"action":"wpcraft_save_sub_category","sub_cat":sub_cat, "main_cat":main_cat, cat_id:cat_id},
			success: function(data) {
				jQuery('.ajax-loader').hide();
				if (data == 'done') {
					jQuery("#myModal").modal("hide");
					jQuery(".succ").text('Successfully Added');
					jQuery(".succ").show();
					
					setTimeout(function(){
						location.reload();
					}, 2000);
				}else if (data == "found") {
					alert('Category Already available');
				}else{
					alert("Error Occured");
				}
			}
		});
	}

	function editCategory(id, main_cat, sub_cat){
		jQuery("#cat_id").val(id);
		jQuery("#main_cat").val(main_cat);
		jQuery("#sub_cat").val(sub_cat);
		jQuery('#myModal').modal('show'); 
	}

	function deleteCategory(id){
		if(confirm("Are you sure?")){
			jQuery.ajax({
				type: 'POST',   
				url: ajaxurl, 
				data: {"action":"wpcraft_delete_sub_category","id":id},
				success: function(data) {
					jQuery('.ajax-loader').hide();
					if (data == "done") {
						jQuery("#" + id).remove();
						setTimeout(function(){
							jQuery(".succ").text('Successfully Deleted');
							jQuery(".succ").show();
						},2500);

					}else{
						jQuery(".err").text('Error Occured');
						jQuery(".err").show();
					}
				}
			});
		}
	}
</script>
	
</div>
