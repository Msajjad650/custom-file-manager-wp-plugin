<?php
/*
Plugin Name: File Manager Plugin
Plugin URI: https://wp-crafts.com
Description: File manager and download files. You can use this shortcode [show_file_manager] to display the tabs this plugin generates on any page or post of your website.
Version: 1.0.0
Author: WPCrafts
Author URI: https://wp-crafts.com/
License: MIT License
*/

defined('ABSPATH') or die('Access Denied');

define('FMAN_PATH', plugin_dir_path(__FILE__));
define('FMAN_URL', plugin_dir_url( __FILE__ ));
define('FMAN', plugin_basename( __FILE__ ));

class File_manager_plugin {
	public function __construct() {
		//add_action( 'admin_init', array( &$this, 'registerSettings' ) );
		add_action( 'admin_menu', array( &$this, 'adminPanelsAndMetaBoxes' ) );
		add_action('wp_ajax_wpcraft_save_sub_category', array($this, 'wpcraft_save_sub_category'));
		add_action('wp_ajax_wpcraft_delete_sub_category', array($this, 'wpcraft_delete_sub_category'));
		add_action('wp_ajax_wpcraft_save_file', array($this, 'wpcraft_save_file'));
		add_action('wp_ajax_wpcraft_delete_file', array($this, 'wpcraft_delete_file'));
		add_action('wp_ajax_wpcraft_get_sub_categories', array($this, 'wpcraft_get_sub_categories'));
		add_action('wp_ajax_nopriv_wpcraft_get_sub_categories', array($this, 'wpcraft_get_sub_categories'));
		
		//phlox_update_categories_sortorder
		add_action('wp_ajax_wpcraft_update_categories_sortorder', array($this, 'wpcraft_update_categories_sortorder'));
		add_action('wp_ajax_nopriv_wpcraft_update_categories_sortorder', array($this, 'wpcraft_update_categories_sortorder'));

		$this->create_categories_table();
		$this->create_file_manager_table();

		
	}

	function wpcraft_save_sub_category(){
		global $wpdb;
        $table_name = $wpdb->prefix.'categories';
        $sub_cat = $_POST['sub_cat'];
        $main_cat = $_POST['main_cat'];
        $cat_id = $_POST['cat_id'];
        if($cat_id == 0){
        	$getfield = $wpdb->get_results($wpdb->prepare("SELECT category_id FROM $table_name WHERE sub_cat = '".$sub_cat."'","")); 
        }else{
        	$getfield = $wpdb->get_results($wpdb->prepare("SELECT category_id FROM $table_name WHERE category_id != '".$cat_id."' AND sub_cat = '".$sub_cat."'","")); 
        }
        
        
        if (count($getfield) == 0) {
        	if($cat_id == 0){
        		$wpdb->query($wpdb->prepare("INSERT INTO $table_name
	            (main_cat, sub_cat) VALUES (%s, %s)",
	            $main_cat, $sub_cat) );	
        	}else{
        		$wpdb->query($wpdb->prepare("UPDATE $table_name SET main_cat = %s, sub_cat = %s WHERE category_id = %d",
	                $main_cat,
	                $sub_cat,
	                $cat_id
	            ));
        	}
        	
            die("done");
        }else{
        	if($cat_id != 0){
        		$wpdb->query($wpdb->prepare("UPDATE $table_name SET main_cat = %s, sub_cat = %s WHERE category_id = %d",
	                $main_cat,
	                $sub_cat,
	                $cat_id
	            ));
	            die('done');
        	}
        	die("found");
        }
        die("err");
	}

	function wpcraft_delete_sub_category(){
		global $wpdb;
        $table_name = $wpdb->prefix.'categories';
        $cat_id = $_POST['id'];

        if($wpdb->query("DELETE FROM $table_name WHERE `category_id` = $cat_id")){
            die('done');
        }else{
            die('not');
        }
	}

	function wpcraft_save_file(){
		global $wpdb;
        $table_name = $wpdb->prefix.'file_manager';
        $file_id = $_POST['file_id'];
        $file_title = $_POST['file_title'];
        $file_link = $_POST['file_link'];
        $file_ext = $_POST['file_ext'];
        $product_cat = $_POST['product_cat'];
        $series_cat = $_POST['series_cat'];
        $model_cat = $_POST['model_cat'];
        $type_cat = $_POST['type_cat'];
        $manufacturer_cat = $_POST['manufacturer_cat'];
        
    	if($file_id == 0){
    		$wpdb->query($wpdb->prepare("INSERT INTO $table_name
            (manufacturer_cat, product_cat, series_cat, model_cat, type_cat, file_ext, file_title, file_link) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
            $manufacturer_cat, $product_cat, $series_cat, $model_cat, $type_cat, $file_ext, $file_title, $file_link) );
            die("done");
    	}else{
    		$wpdb->query($wpdb->prepare("UPDATE $table_name SET manufacturer_cat = %s, product_cat = %s, series_cat = %s, 
                model_cat = %s, type_cat = %s, file_ext = %s, file_title = %s, 
                file_link = %s WHERE file_id = %d",
                $manufacturer_cat,
                $product_cat,
                $series_cat,
                $model_cat,
                $type_cat,
                $file_ext,
                $file_title,
                $file_link,
                $file_id
            ));
            die("done");
    	}
        
        die("err");
	}

	function wpcraft_delete_file(){
		global $wpdb;
        $table_name = $wpdb->prefix.'file_manager';
        $file_id = $_POST['id'];

        if($wpdb->query("DELETE FROM $table_name WHERE `file_id` = $file_id")){
            die('done');
        }else{
            die('not');
        }
	}

	function wpcraft_get_sub_categories(){
		
		$type = $_POST['type'];
		$manufacturer_cat = $_POST['manufacturer_data'];
		$product_cat = $_POST['product_data'];
		$series_cat = $_POST['series_data'];
		$model_cat = $_POST['models_data'];
		$type_cat = $_POST['type_data'];

		$data = '';

		global $wpdb;
		$table_name = $wpdb->prefix.'file_manager';
		if($type == "manufacturer"){
		    $manufacturer_data = $wpdb->get_results($wpdb->prepare("SELECT `product_cat` FROM $table_name WHERE `manufacturer_cat` = '".$manufacturer_cat."' GROUP BY `product_cat`",""));
		  //  print_r($manufacturer_data)
			if (count($manufacturer_data) > 0 ) {
				$prod = 'product';
                $prod = "'".$prod."'";
                $manufacturer_cat = "'".$manufacturer_cat."'";
				foreach ($manufacturer_data as $key => $value) {
					$cat = "'".$value->product_cat."'";
					$data.= '<li class="lines-select fman-list-product" onclick="showNextCategories(this, '.$prod.','.$manufacturer_cat.','.$cat.',0,0,0)">'.$value->product_cat.'</li>';	
				}				
			}else{
				$data = "No Data Found";
			}
		} else if($type == "product"){
			$product_data = $wpdb->get_results($wpdb->prepare("SELECT `series_cat` FROM $table_name WHERE `manufacturer_cat` = '".$manufacturer_cat."' AND `product_cat` = '".$product_cat."' GROUP BY `series_cat`",""));
			if (count($product_data) > 0 ) {
				$prod = 'series';
                $prod = "'".$prod."'";
                $manufacturer_cat = "'".$manufacturer_cat."'";
                $product_cat = "'".$product_cat."'";
				foreach ($product_data as $key => $value) {
					$cat = "'".$value->series_cat."'";
					$data.= '<li class="lines-select fman-list-series" onclick="showNextCategories(this, '.$prod.','.$manufacturer_cat.','.$product_cat.','.$cat.',0,0)">'.$value->series_cat.'</li>';	
				}				
			}else{
				$data = "No Data Found";
			}
		} else if ($type == "series") {
			$series_data = $wpdb->get_results($wpdb->prepare("SELECT `model_cat` FROM $table_name WHERE `manufacturer_cat` = '".$manufacturer_cat."' AND `product_cat` = '".$product_cat."' AND `series_cat` = '".$series_cat."' GROUP BY `model_cat`",""));
			if (count($series_data) > 0 ) {
				$prod = 'model';
                $prod = "'".$prod."'";
                $manufacturer_cat = "'".$manufacturer_cat."'";
                $product_cat = "'".$product_cat."'";
                $series_cat = "'".$series_cat."'";
				foreach ($series_data as $key => $value) {
					$cat = "'".$value->model_cat."'";
					$data.= '<li class="lines-select fman-list-model" onclick="showNextCategories(this, '.$prod.','.$manufacturer_cat.','.$product_cat.','.$series_cat.','.$cat.',0)">'.$value->model_cat.'</li>';	
				}				
			}else{
				$data = "No Data Found";
			}
		} else if ($type == "model") {
			$model_data = $wpdb->get_results($wpdb->prepare("SELECT `type_cat` FROM $table_name WHERE `manufacturer_cat` = '".$manufacturer_cat."' AND `product_cat` = '".$product_cat."' AND `series_cat` = '".$series_cat."' AND `model_cat` = '".$model_cat."' GROUP BY `type_cat`",""));
			if (count($model_data) > 0 ) {
				$prod = 'type';
                $prod = "'".$prod."'";
                $manufacturer_cat = "'".$manufacturer_cat."'";
                $product_cat = "'".$product_cat."'";
                $series_cat = "'".$series_cat."'";
                $model_cat = "'".$model_cat."'";
				foreach ($model_data as $key => $value) {
					$cat = "'".$value->type_cat."'";
					$data.= '<li class="lines-select fman-list-type" onclick="showNextCategories(this, '.$prod.','.$manufacturer_cat.','.$product_cat.','.$series_cat.','.$model_cat.','.$cat.')">'.$value->type_cat.'</li>';	
				}				
			}else{
				$data = "No Data Found";
			}
		} else {
			$file_data = $wpdb->get_results($wpdb->prepare("SELECT `file_ext`,`file_title`,`file_link` FROM $table_name WHERE `manufacturer_cat` = '".$manufacturer_cat."' AND `product_cat` = '".$product_cat."' AND `series_cat` = '".$series_cat."' AND `model_cat` = '".$model_cat."' AND `type_cat` = '".$type_cat."'",""));

			if (count($file_data) > 0 ) {
				foreach ($file_data as $key => $value) {
					$data.= '<tr>';
					$data.= '<td>'.$value->file_title.'</td>';
					$data.= '<td><img style="display:none;" src="'.$this->getExtensionIcon($value->file_ext).'">'.strtoupper($value->file_ext).'</td>';
					$data.= '<td><a href="'.$value->file_link.'">Download</a></td>';
					$data.= '</tr>';
				}
			}
		}
		echo $data;
		die();
	}
	
	function getExtensionIcon($ext){
	    $data = '';
	    switch ($ext) {
            case "docx":
                $data = "https://img.icons8.com/nolan/30/000000/ms-word.png";
                break;
            case "pdf":
                $data = "https://img.icons8.com/nolan/30/000000/pdf.png";
                break;
            case "jpg":
                $data = "https://img.icons8.com/nolan/30/000000/jpg.png";
                break;
            case "png":
                $data = "https://img.icons8.com/nolan/30/000000/png.png";
                break;
            case "tiff":
                $data = "https://img.icons8.com/nolan/30/000000/tif.png";
                break;
            case "dwg":
                $data = "https://img.icons8.com/nolan/30/000000/autocad.png";
                break;
            case "xlsx":
                $data = "https://img.icons8.com/nolan/30/000000/xls.png";
                break;
            case "cdr":
                $data = FMAN_URL."images/cdr.png";
                break;
            case "rfa":
                $data = FMAN_URL."images/rfa.png";
                break;
            case "ai":
                $data = "https://img.icons8.com/nolan/30/000000/ai.png";
                break;
            case "eps":
                $data = "https://img.icons8.com/nolan/30/000000/eps.png";
                break;
            case "zip":
                $data = "https://img.icons8.com/nolan/30/000000/zip.png";
                break;
            default:
                $data = "https://img.icons8.com/nolan/30/000000/txt.png";
        }
        
        return $data;
	}
	
	function wpcraft_update_categories_sortorder(){
	    print_r($_POST);
	    global $wpdb;
        $fieldids = $_POST['fieldids'];
        $counter = 1;
        $table_name = $wpdb->prefix.'categories';
        foreach ($fieldids as $fieldid) {
            $wpdb->query($wpdb->prepare("UPDATE $table_name SET sort_order = %d WHERE category_id = %d", $counter, intval($fieldid) ));            
            $counter = $counter + 1;    
        }
	}

	function adminPanelsAndMetaBoxes() {
    	add_menu_page( 
    		'File Manager Plugin',
    	 	'File Manager', 
    	 	'manage_options', 
    	 	'fman_plugin', 
    	 	array($this, 'manage_files'), 
    	 	'dashicons-store', 
    	 	110 
    	 );
        add_submenu_page( 
            'fman_plugin', 
            'Manage Categories', 
            'Manage Categories', 
            'manage_options', 
            'manage-categories',
            array($this, 'manage_categories_page'),
            ''
        );
	}

	public function manage_files(){
    	require_once FMAN_PATH.'templates/manage_files.php';
    }

    public function manage_categories_page(){
    	require_once FMAN_PATH.'templates/manage_categories_page.php';
    }

    public function create_categories_table(){
    	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'categories';
		if (count($wpdb->get_var('SHOW TABLES LIKE "$table_name"')) == 0){
			$create_table_query = "CREATE TABLE `".$table_name."` (
					  `category_id` int(25) NOT NULL AUTO_INCREMENT,
					  `main_cat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `sub_cat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					   `sort_order` int(25) NOT NULL,
					  PRIMARY KEY (`category_id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci"; 

			dbDelta($create_table_query);
		}
	}

	public function create_file_manager_table(){
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'file_manager';
		if (count($wpdb->get_var('SHOW TABLES LIKE "$table_name"')) == 0){
			$create_table_query = "CREATE TABLE `".$table_name."` (
					  `file_id` int(25) NOT NULL AUTO_INCREMENT,
					  `manufacturer_cat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `product_cat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `series_cat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `model_cat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `type_cat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `file_ext` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `file_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `file_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  PRIMARY KEY (`file_id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci"; 

			dbDelta($create_table_query);
		}
	}
}

$file_manager_plugin = new File_manager_plugin();



function show_file_manager_front_end() {
 
	ob_start(); ?>
		
	<?php
	require_once FMAN_PATH.'templates/show_files.php';
	return ob_get_clean();
}

add_shortcode('show_file_manager', 'show_file_manager_front_end');